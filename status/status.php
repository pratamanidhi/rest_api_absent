<?php
header("Content-Type: application/json");
header("Acess-Control-Allow-Origin: *");
require_once "../db_config.php";
$query = "SELECT * FROM t_status";
$result = mysqli_query($conn, $query) or die ("Query failed");
$count = mysqli_num_rows($result);
if($count > 0){
	$row = mysqli_fetch_all($result,MYSQLI_ASSOC);
    $response = array(
        "status" => 1,
        "message" => "Get list success",
        "data" => $row
    );
	echo json_encode($response);
}
else{
	echo json_encode(array("message" => "No Product Found.", "status" => false));
}
?>