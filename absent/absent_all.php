<?php 
header("Content-Type: application/json");
header("Acess-Control-Allow_Origin: *");
require_once "../db_config.php";
$query = "SELECT * FROM t_absen";
$result = mysqli_query($conn, $query) or die (json_encode(
    array(
        "message" => "false query",
        "user_id" => $id,
        "query" => $query
    )
    ));
    $count = mysqli_num_rows($result);
    if($count > 0) {
        $row = mysqli_fetch_all($result, MYSQLI_ASSOC);
        $response = array(
            "status" => 1,
            "message" => "success",
            "data" => $row
        );
        echo json_encode($response);
    }else{
        echo json_encode(
            array(
                "status" => 0,
                "message" => "error",
                "data" => "user not found" 
            )
        );
    }
?>