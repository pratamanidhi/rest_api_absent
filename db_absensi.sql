-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 28, 2021 at 05:24 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_absensi`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_absen`
--

CREATE TABLE `t_absen` (
  `idabsen` int(10) NOT NULL,
  `nip` varchar(500) NOT NULL,
  `jam_masuk` varchar(500) NOT NULL,
  `jam_keluar` varchar(500) NOT NULL,
  `is_in` tinyint(1) NOT NULL,
  `is_out` tinyint(1) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_absen`
--

INSERT INTO `t_absen` (`idabsen`, `nip`, `jam_masuk`, `jam_keluar`, `is_in`, `is_out`, `created`) VALUES
(1, '9', '', '', 0, 0, '2021-10-21 15:33:22'),
(2, '9', '21/10/2021 22:35:39', '', 1, 0, '2021-10-21 15:35:48'),
(3, '9', '', '', 0, 0, '2021-10-21 15:36:01');

-- --------------------------------------------------------

--
-- Table structure for table `t_company_location`
--

CREATE TABLE `t_company_location` (
  `id` int(11) NOT NULL,
  `location_name` varchar(255) NOT NULL,
  `lat` varchar(255) NOT NULL,
  `long` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_company_location`
--

INSERT INTO `t_company_location` (`id`, `location_name`, `lat`, `long`) VALUES
(1, 'marikh', '-7.322800', '110.503900'),
(2, 'FTI', '-7.2962', '110.4918'),
(5, 'kos', '-7.3172201', '110.4957465');

-- --------------------------------------------------------

--
-- Table structure for table `t_days`
--

CREATE TABLE `t_days` (
  `id` int(11) NOT NULL,
  `days` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_days`
--

INSERT INTO `t_days` (`id`, `days`) VALUES
(1, 'senin'),
(2, 'selasa'),
(3, 'rabu'),
(4, 'kamis'),
(5, 'jumat'),
(6, 'sabtu'),
(7, 'minggu');

-- --------------------------------------------------------

--
-- Table structure for table `t_lokasi`
--

CREATE TABLE `t_lokasi` (
  `id_lokasi` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `longitude` varchar(500) NOT NULL,
  `langitude` varchar(500) NOT NULL,
  `absent_kind` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_lokasi`
--

INSERT INTO `t_lokasi` (`id_lokasi`, `id_user`, `longitude`, `langitude`, `absent_kind`, `date`, `created`) VALUES
(1, 1, '7.00', '10.00', 'in', '12-12-2021', '2021-09-04 17:11:10'),
(2, 1, '10.000', '7.000', 'in', '20:00', '2021-09-04 17:12:11'),
(3, 9, '11', '11', 'in', 'dfaf', '2021-10-08 13:02:57'),
(4, 9, '110.4965838', '-7.3200438', 'in', 'sdvssdv', '2021-10-08 13:11:54'),
(5, 9, '110.4965838', '-7.3200438', 'in', '08/10/2021 11:02:14', '2021-10-08 16:02:25'),
(6, 9, '110.4965838', '-7.3200438', 'in', '08/10/2021 11:12:34', '2021-10-08 16:12:38'),
(7, 9, '110.4965838', '-7.3200438', 'in', '08/10/2021 11:13:24', '2021-10-08 16:13:58'),
(8, 9, '110.4965838', '-7.3200438', 'in', '08/10/2021 11:15:34', '2021-10-08 16:15:40'),
(9, 9, '110.4965838', '-7.3200438', 'out', '08/10/2021 11:19:03', '2021-10-08 16:19:09'),
(10, 9, '110.4965838', '-7.3200438', 'out', '08/10/2021 11:19:03', '2021-10-08 16:20:40'),
(11, 9, '110.4965838', '-7.3200438', 'out', '08/10/2021 11:21:39', '2021-10-08 16:21:42'),
(12, 9, '110.4965838', '-7.3200438', 'out', '08/10/2021 11:22:02', '2021-10-08 16:22:04'),
(13, 9, '110.4965809', '-7.3200344', 'out', '21/10/2021 08:41:10', '2021-10-21 13:41:32'),
(14, 9, '110.4965809', '-7.3200344', 'out', '21/10/2021 08:41:53', '2021-10-21 14:05:56'),
(15, 9, '110.4965809', '-7.3200344', 'out', '21/10/2021 09:07:18', '2021-10-21 14:07:24'),
(16, 9, '110.4965809', '-7.3200344', 'in', '21/10/2021 09:10:24', '2021-10-21 14:10:32'),
(17, 9, '110.4965809', '-7.3200344', 'out', '21/10/2021 09:13:30', '2021-10-21 14:13:36'),
(18, 9, '110.4965809', '-7.3200344', 'in', '21/10/2021 22:27:17', '2021-10-21 15:27:35'),
(19, 9, '110.4965809', '-7.3200344', 'in', '21/10/2021 22:31:19', '2021-10-21 15:31:28'),
(20, 9, '110.4965809', '-7.3200344', 'in', '21/10/2021 22:33:22', '2021-10-21 15:33:32'),
(21, 9, '110.4965809', '-7.3200344', 'in', '21/10/2021 22:33:22', '2021-10-21 15:33:43'),
(22, 9, '110.4965809', '-7.3200344', 'in', '21/10/2021 22:35:39', '2021-10-21 15:35:48');

-- --------------------------------------------------------

--
-- Table structure for table `t_pegawai`
--

CREATE TABLE `t_pegawai` (
  `NIP` int(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `telepon` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_status` int(11) NOT NULL,
  `user_job` int(11) NOT NULL,
  `days` int(11) NOT NULL,
  `job_locations_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_pegawai`
--

INSERT INTO `t_pegawai` (`NIP`, `nama`, `alamat`, `telepon`, `username`, `password`, `user_status`, `user_job`, `days`, `job_locations_id`) VALUES
(4, 'dfhd', 'dhdf', 'dfhdf', 'dhdfh', 'dhdf', 2, 1, 1, 0),
(5, 'nidhi', 'salatiga', '234567', 'admin', 'admin', 1, 1, 1, 0),
(7, 'aa', '1', '1', '1', '1', 2, 1, 1, 0),
(8, 'heheh', 'heheh', 'heheh', 'heheh', 'heheh', 2, 1, 1, 0),
(9, 'sean', 'salatiga', '1366', 'sean', '12345', 2, 1, 3, 1),
(12, 'ghdf', 'dfhdfh', 'dfhdfh', 'dfhdh', 'dfhdfhd', 1, 1, 0, 2),
(13, 'ghdf', 'dfhdfh', 'dfhdfh', 'dfhdh', 'dfhdfhd', 1, 1, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `t_pekerjaan`
--

CREATE TABLE `t_pekerjaan` (
  `id_pekerjaan` int(11) NOT NULL,
  `pekerjaan` varchar(500) NOT NULL,
  `gaji` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_pekerjaan`
--

INSERT INTO `t_pekerjaan` (`id_pekerjaan`, `pekerjaan`, `gaji`) VALUES
(1, 'Koki', '15000'),
(2, 'Kasir', '10000'),
(3, 'Pelayan', '5000');

-- --------------------------------------------------------

--
-- Table structure for table `t_status`
--

CREATE TABLE `t_status` (
  `id` int(11) NOT NULL,
  `status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_status`
--

INSERT INTO `t_status` (`id`, `status`) VALUES
(1, 'admin'),
(2, 'karyawan');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_absen`
--
ALTER TABLE `t_absen`
  ADD PRIMARY KEY (`idabsen`);

--
-- Indexes for table `t_company_location`
--
ALTER TABLE `t_company_location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_days`
--
ALTER TABLE `t_days`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_lokasi`
--
ALTER TABLE `t_lokasi`
  ADD PRIMARY KEY (`id_lokasi`);

--
-- Indexes for table `t_pegawai`
--
ALTER TABLE `t_pegawai`
  ADD PRIMARY KEY (`NIP`);

--
-- Indexes for table `t_pekerjaan`
--
ALTER TABLE `t_pekerjaan`
  ADD PRIMARY KEY (`id_pekerjaan`);

--
-- Indexes for table `t_status`
--
ALTER TABLE `t_status`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_absen`
--
ALTER TABLE `t_absen`
  MODIFY `idabsen` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `t_company_location`
--
ALTER TABLE `t_company_location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `t_days`
--
ALTER TABLE `t_days`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `t_lokasi`
--
ALTER TABLE `t_lokasi`
  MODIFY `id_lokasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `t_pegawai`
--
ALTER TABLE `t_pegawai`
  MODIFY `NIP` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `t_pekerjaan`
--
ALTER TABLE `t_pekerjaan`
  MODIFY `id_pekerjaan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `t_status`
--
ALTER TABLE `t_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
